import api from './config';

export default {
    getAllReviews,
    newReview
}

function getAllReviews() {
    return api().get('/reviews')
}

function newReview(review, config = null) {
    return api().post('/reviews', review, config)
}
