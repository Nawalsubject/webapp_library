import Api from "../../services/api/index";

const state = {
    books: [],
};

const getters = {
    getBooks: state => state.books
};

const mutations = {
    SET_BOOKS: (state, books) => {
        state.books = books
    },
    ADD_BOOK: (state, books) => {
        state.books = books
    },
    DELETE_BOOK: (state, book) => {
        state.books.splice(book, 1)
    },
    UPDATE_BOOK: (state, book) => {
        state.books[book.id] = book
    }
};

const actions = {
     setBooks: async ( store ) => {
        const response = await Api.bookApi.getAllBooks();
        store.commit('SET_BOOKS', response.data.reverse());
    },
    addBook: async (store, book) => {
        const response = await Api.bookApi.newBook(book);
        store.commit('ADD_BOOK', response.data);
    },
    deleteBook: async (store, book) => {
         await Api.bookApi.removeBook(book.id);
         store.commit('DELETE_BOOK', book)
    },
    updateBook: async (store, book) => {
         await Api.bookApi.editBook(book);
         store.commit('UPDATE_BOOK', book)
    }
};

export default {
    namespaced : true,
    state,
    mutations,
    actions,
    getters
}
