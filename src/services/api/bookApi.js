import api from './config'

export default {
    getAllBooks,
    newBook,
    editBook,
    removeBook
}

function getAllBooks() {
    return api().get('/books')
}

function newBook(book, config = null) {
    return api().post('/livres', book, config)
}

function removeBook(bookId) {
    return api().delete('/livres/' + bookId)
}

function editBook(book) {
    return api().put('/livres/' + book.id, book)
}
