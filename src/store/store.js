import Vue from 'vue';
import Vuex from 'vuex';
import booksModule from "./modules/booksModule";
import reviewsModule from "./modules/reviewsModule";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    booksModule,
    reviewsModule
  }
})
