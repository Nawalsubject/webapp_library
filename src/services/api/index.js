import bookApi from "./bookApi";
import reviewApi from "./reviewApi";

export default {
    bookApi,
    reviewApi
}
