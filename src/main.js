import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import AnimateCss from 'animate.css'
import VueCarousel from 'vue-carousel'
import './filters/filters'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';


library.add(faUserCircle)
library.add(faEye)
library.add(faEdit)
library.add(faTimesCircle)
library.add(faSignOutAlt)
library.add(faTrashAlt)
library.add(faStar)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueCarousel)
Vue.use(AnimateCss);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
