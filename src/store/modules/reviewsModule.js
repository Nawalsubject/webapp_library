import Api from "../../services/api/index";

const state = {
    reviews: [],
};

const getters = {
    getReviews: state => {
        return state.reviews
    }
};

const mutations = {
    SET_REVIEWS: (state, reviews) => {
        state.reviews = reviews
    }
};

const actions = {
    setReviews: async (store) => {
        const response = await Api.reviewApi.getAllReviews()
        store.commit('SET_REVIEWS', response.data)
    },
};

export default {
    namespaced : true,
    state: state,
    mutations:mutations,
    actions:actions,
    getters :getters
}
